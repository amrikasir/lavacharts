# Lava.js

[![Build Status](https://travis-ci.org/lavacharts/lava.js.svg?branch=master)](https://travis-ci.org/lavacharts/lava.js)

Lava.js is the javsacript module that accompanies the [Lavacharts PHP library](https://github.com/kevinkhill/lavacharts)

## New!
As of v4.0, Lava.js can be used independently from Lavacharts if you feel so inclined.

Clone or download the repo and check out the examples folder to see how easy it can be to make charts.
